(define-package "org-ref" "1.1.1" "citations, cross-references and bibliographies in org-mode"
  '((dash "2.11.0")
    (htmlize "1.51")
    (ivy "0.8.0")
    (hydra "0.13.2")
    (key-chord "0")
    (s "1.10.0")
    (f "0.18.0")
    (emacs "24.4")))

;; Local Variables:
;; no-byte-compile: t
;; End:
